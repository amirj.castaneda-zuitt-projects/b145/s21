// ES6 Updates

// Math.pow() -. return the base to the exponent power, as in base^exponent.

let fullName = ['Juan', 'Dela', 'Cruz'];

let [firstName, middleName, lastName] = fullName;
console.log(`Hello, my first name is ${firstName} and my last name is ${lastName}`);